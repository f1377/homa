import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:homa/models/person.dart';

class PersonService {
  static const String kImageCardsPath = 'assets/images/cards/';
  static const String kSoundsPath = 'assets/sounds/';

  Future<List<String>> getImages(BuildContext context) async {
    final manifestContent =
        await DefaultAssetBundle.of(context).loadString('AssetManifest.json');

    final Map<String, dynamic> manifestMap = json.decode(manifestContent);

    final imagePaths = manifestMap.keys
        .where((String key) => key.contains(kImageCardsPath))
        .toList();

    return imagePaths;
  }

  Future<List<String>> getSounds(BuildContext context) async {
    final manifestContent =
        await DefaultAssetBundle.of(context).loadString('AssetManifest.json');

    final Map<String, dynamic> manifestMap = json.decode(manifestContent);

    final soundPaths = manifestMap.keys
        .where((String key) => key.contains(kSoundsPath))
        .toList();

    return soundPaths;
  }

  Future<List<Person>> createPersonsWithSound(
      List<String> soundPaths, List<String> imagePaths) async {
    List<Person> persons = Iterable.generate(imagePaths.length, (int index) {
      String name =
          getNameFromPath(imagePaths[index], kImageCardsPath, '.jpeg');
      String imagePath = imagePaths[index];
      String soundPath = soundPaths[index];

      return Person(
        name: name[0].toUpperCase() + name.substring(1),
        imagePath: imagePath,
        soundPath: soundPath,
      );
    }).toList();

    return persons;
  }

  Future<List<Person>> createPersons(
      List<String> soundPaths, List<String> imagePaths) async {
    List<Person> persons = [];
    for (String path in imagePaths) {
      String name = getNameFromPath(path, kImageCardsPath, '.jpeg');
      String soundPath = soundPaths.firstWhere(
        (path) => path.contains(name),
        orElse: () => '',
      );

      Person newPerson = Person(
        name: name[0].toUpperCase() + name.substring(1),
        imagePath: path,
        soundPath: soundPath,
      );

      persons.add(newPerson);
    }

    return persons;
  }

  void printPersons(List<Person> persons) {
    print('-----------------------------------------------');
    for (Person person in persons) {
      print('person.name: ${person.name}');
      print('person.path: ${person.imagePath}');
      print('person.soundPath: ${person.soundPath}');
    }
    print('-----------------------------------------------');
  }

  String getNameFromPath(String path, String start, String end) {
    String name = path.replaceAll(start, '');
    name = name.replaceAll(end, '');

    return name;
  }
}
