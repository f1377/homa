import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:homa/constants.dart';
import 'package:homa/models/person.dart';

class OverviewScreen extends StatefulWidget {
  final List<Person> persons;
  final AudioPlayer player;

  const OverviewScreen({
    Key key,
    this.persons,
    this.player,
  }) : super(key: key);

  @override
  _OverviewScreenState createState() => _OverviewScreenState();
}

class _OverviewScreenState extends State<OverviewScreen> {
  AudioPlayer _player;
  List<Person> _persons = [];
  List<Widget> _imagesToDisplay = [];
  bool _soundIsPlaying = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _setPersons();
  }

  void _playSound(Person person) async {
    if (_soundIsPlaying) {
      await this._player.pause();
      this._soundIsPlaying = false;
    } else {
      await this._player.play(person.soundPath, isLocal: true);
      this._soundIsPlaying = true;
    }
  }

  Container _setGridListItem(Person person) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: GestureDetector(
        onTap: () {
          _playSound(person);
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _setRoundImage(person),
            _setImageName(person),
          ],
        ),
      ),
    );
  }

  Container _setRoundImage(Person person) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        color: Colors.red.shade900,
        borderRadius: BorderRadius.all(Radius.circular(50)),
        border: Border.all(
          color: kDarkRedOP05,
          width: 3,
        ),
        image: DecorationImage(
          image: AssetImage(person.imagePath),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Container _setImageName(Person person) {
    return Container(
      height: 30,
      child: Center(
        child: Text(
          person.name,
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }

  Future<void> _setPersons() async {
    this._player = widget.player;
    this._persons = widget.persons;

    setState(() {
      this._imagesToDisplay = this._persons.map((person) {
        return _setGridListItem(person);
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 3,
        children: this._imagesToDisplay,
        childAspectRatio: 0.8,
        controller: ScrollController(keepScrollOffset: false),
        mainAxisSpacing: 12,
        scrollDirection: Axis.vertical,
      ),
    );
  }
}
