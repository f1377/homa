import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:homa/constants.dart';
import 'package:homa/models/person.dart';

class HomaScreen extends StatefulWidget {
  final List<Person> persons;
  final AudioPlayer player;

  const HomaScreen({
    Key key,
    this.persons,
    this.player,
  }) : super(key: key);

  @override
  _HomaScreenState createState() => _HomaScreenState();
}

class _HomaScreenState extends State<HomaScreen> {
  final String kBackgroundImage = 'assets/images/background/blumen.jpeg';
  AudioPlayer _player;
  final CardController _cardController = CardController();

  List<Person> _persons = [];
  int _cardsLength = 0;
  bool _soundIsPlaying = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _setPersons();
  }

  void _playSound(Person person) async {
    if (_soundIsPlaying) {
      await this._player.pause();
      this._soundIsPlaying = false;
    } else {
      await this._player.play(person.soundPath, isLocal: true);
      this._soundIsPlaying = true;
    }
  }

  Future<void> _setPersons() async {
    this._player = widget.player;
    setState(() {
      this._persons = widget.persons;
    });
    this._cardsLength = this._persons.length;
  }

  Container _buildCards(BuildContext context, int index) {
    int realIndex = (this._cardsLength + index) % this._persons.length;

    return Container(
      child: GestureDetector(
        onTap: () {
          Person person = this._persons[realIndex];
          _playSound(person);
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
            side: BorderSide(
              color: kDarkRed,
              width: 1.5,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 230,
                height: 230,
                child: Image.asset('${this._persons[realIndex].imagePath}'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 2, bottom: 2),
                child: Text(
                  '${this._persons[realIndex].name}',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
          // Image.asset('${this._persons[realIndex].imagePath}'),
        ),
      ),
    );
  }

  Container _cardSwiper() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(kBackgroundImage),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.6,
            child: new TinderSwapCard(
              swipeUp: true,
              swipeDown: true,
              orientation: AmassOrientation.BOTTOM,
              totalNum: this._cardsLength,
              stackNum: 3,
              swipeEdge: 4.0,
              maxWidth: MediaQuery.of(context).size.width * 0.9,
              maxHeight: MediaQuery.of(context).size.width * 0.9,
              minWidth: MediaQuery.of(context).size.width * 0.8,
              minHeight: MediaQuery.of(context).size.width * 0.8,
              cardBuilder: _buildCards,
              cardController: this._cardController,
              swipeCompleteCallback:
                  (CardSwipeOrientation orientation, int index) {
                setState(() {
                  this._cardsLength++;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _cardSwiper(),
    );
  }
}
