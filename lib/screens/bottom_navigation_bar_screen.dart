import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:homa/constants.dart';
import 'package:homa/models/person.dart';
import 'package:homa/screens/homa_screen.dart';
import 'package:homa/screens/overview_screen.dart';
import 'package:homa/services/person_service.dart';

class BottomNavigationBarScreen extends StatefulWidget {
  @override
  _BottomNavigationBarScreenState createState() =>
      _BottomNavigationBarScreenState();
}

class _BottomNavigationBarScreenState extends State<BottomNavigationBarScreen>
    with SingleTickerProviderStateMixin {
  final PersonService _personService = PersonService();
  final AudioCache _audioCache = AudioCache(prefix: '');
  final AudioPlayer _player = AudioPlayer();

  int _selectPageIndex = 0;
  List<Map<String, dynamic>> _pages;

  @override
  void initState() {
    super.initState();
    this._pages = [
      {'page': Container()},
      {'page': Container()},
    ];
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    _setImagesAndPersons();
  }

  Future<void> _setImagesAndPersons() async {
    List<String> imagePaths = await this._personService.getImages(context);
    List<String> soundPaths = await this._personService.getSounds(context);
    List<Uri> soundFiles = await this._audioCache.loadAll(soundPaths);
    List<String> soundFilesPaths = soundFiles.map((file) => file.path).toList();
    List<Person> persons =
        await this._personService.createPersons(soundFilesPaths, imagePaths);

    setState(() {
      this._pages = [
        {'page': HomaScreen(persons: persons, player: this._player)},
        {'page': OverviewScreen(persons: persons, player: this._player)},
      ];
    });
  }

  void _selectPage(int index) {
    setState(() {
      this._selectPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = this._pages[this._selectPageIndex]['page'];

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      body: body,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        selectedLabelStyle: TextStyle(fontSize: 24),
        backgroundColor: kDarkRedOP07,
        unselectedItemColor: Colors.white54,
        selectedItemColor: Colors.white,
        currentIndex: this._selectPageIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.wallet_giftcard_sharp),
            label: 'Karten',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.image),
            label: 'Übersicht',
          ),
        ],
      ),
    );
  }
}
